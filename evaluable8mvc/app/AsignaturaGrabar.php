<?php
session_start();
if (!$_SESSION['SesionValida']) {
    header("Location: index.php");
}
include_once("funciones.php");
include_once("Modelo.php");
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        cabecera();
        echo "<h2>Asignatura Grabar</h2>";

        //**************************************
        function leerAsignatura() {
            $id = recoge("id");
            $nombre = recoge("nombre");
            $horas = recoge("horas");
            $profesorid = recoge("profesor");
            //echo "Leido form:  Asig:" . $nombre . " Profesor:" . $profesorid . "<br>";
            $profesor_ = new Profesor($profesorid, "");
            $modelo = obtenerModelo();
            $profesor = $modelo->getProfesor($profesor_);
            $asignatura = new Asignatura($id, $nombre, $horas, $profesor);

            return $asignatura;
        }

        //*****************************
        //*  main
        //*****************************

        $asignatura = leerAsignatura();

        echo "Leido Asign: " . $asignatura->getId()
        . " " . $asignatura->getNombre()
        . " " . $asignatura->getProfesor()->getId()
        . "<br>";

        if ($asignatura->getNombre() == "") {
            echo "Error: Nombre asignatura vacio" . "<br>";
        } else if ($asignatura->getHoras() == "") {
            echo "Error: Horas vacio" . "<br>";
        } else if ($asignatura->getProfesor()->getId() == "") {
            echo "Error: Nombre profesor vacio" . "<br>";
        } else {
            $modelo = obtenerModelo();
            if ($modelo->grabarAsignatura($asignatura)) {
                echo "Grabado: " . $asignatura->getNombre() . "<br>";
            } else {
                echo "No grabado: " . $asignatura->getNombre() . "<br>";
            }
        }

        inicio();
        pie();
        ?>
    </body>
</html>
