<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <title><?php echo Config::$titulo ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="<?php echo 'css/' . Config::$mvc_vis_css ?>" />

    </head>
    <body>
        <div id="cabecera">
            <h1><?php echo Config::$titulo ?></h1>

        </div>

        <div id="contenido">
            <?php echo $contenido ?>
        </div>

        <div id="pie">
            <hr/>
            <div align="left">
                <?php
                echo "<pre>" . Config::$empresa . " " . Config::$grupo . " ";
                echo Config::$curso . " " . Config::$fecha . "</pre>";
                ?>
            </div>
        </div>
    </body>
</html>
