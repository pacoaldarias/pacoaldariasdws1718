<?php
	class Session {
	 	// Métodos mágicos
	 	function __construct($permitirInactivo = 5) {
	 		session_start();
	 		$this->permitirInactivo = $permitirInactivo * 60;
	 	}

	 	function __set($var,$valor) {
			$_SESSION[$var] = $valor;
	 	}

	 	function __get($var) {
	 		if (isset($_SESSION[$var])){
				return $_SESSION[$var];
			} else {
				return null;
			}
	 	}
	 	// Métodos normales
	 	public function cerrarSesion() {
	 		session_destroy();
			if (ini_get("session.use_cookies")) {
				$params = session_get_cookie_params();
				setcookie(session_name(),'',time()-42000,$params["path"],$params["domain"],$params["secure"],$params["httponly"]);
			}
	 	}

	 	public function usuarioActivo() {
	 		$tiempoTranscurrido = time() - $this->ultimaActividad;
			if ($tiempoTranscurrido < $this->permitirInactivo) {
				return true;
			} else {
				return false;
			}
	 	}

	 	public function guardarActividad() {
	 		$this->ultimaActividad = time();
	 	}

	 	public function sesionAbierta() {
	 		if (isset($_SESSION['usuario'])) {
	 			return true;
	 		} else {
	 			return false;
	 		}
	 	}
	}
?>